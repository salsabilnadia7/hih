<?php
    class Home extends CI_Controller {
      
            function __construct(){
                parent::__construct();
                $this->load->model('homehih');
            }
            
        
            public function index(){ 
                $data = array(
                        'homes' => $this->homehih->get(),
                        'event' => $this->homehih->gett(),
                        'upcoming' => $this->homehih->getsoon(),
                        'preview' => $this->homehih->preview()
                );
                $this->load->view("layout/header");
                $this->load->view("home", $data);
                $this->load->view("layout/footer");
        
            }

            public function detail($id = 0){
                
                $data = array(
                    'det' =>$this->homehih->detail($id)
                );
    
                $this->load->view("layout/header");
                $this->load->view("detailevent", $data);
                $this->load->view("layout/footer");
            }
          
            
        
    }
?>