<?php
    class Upcoming extends CI_Controller {
      
            function __construct(){
                parent::__construct();
                $this->load->model('upcominghih');
            }
            
        
            public function index(){ 
                $data = array(
                        'upcoming' => $this->upcominghih->get()
                       
                );
                $this->load->view("layout/header");
                $this->load->view("upcoming", $data);
                $this->load->view("layout/footer");
        
            }

          
            
        
    }
?>