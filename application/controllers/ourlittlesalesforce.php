<?php
    class Ourlittlesalesforce extends CI_Controller {
      
            function __construct(){
                parent::__construct();
                $this->load->model('Salesforcehih');
                
            }
            
        
            public function index(){ 

                $data = array(
                    'salesforce1' => $this->Salesforcehih->get1(),
                    'salesforce2' => $this->Salesforcehih->get2(),
                    'salesforce3' => $this->Salesforcehih->get3(),
                    'salesforce4' => $this->Salesforcehih->get4(),
                    
            );
              
                $this->load->view("layout/header");
                $this->load->view("ourlittlesalesforce", $data);
                $this->load->view("layout/footer");
        
            }

          
            
        
    }
?>