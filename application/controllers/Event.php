<?php
    class Event extends CI_Controller {
      
            function __construct(){
                parent::__construct();
                $this->load->model('eventhih');
            }
            
        
            public function index(){ 
                $data = array(
                        'event' => $this->eventhih->get(),
                        'events' => $this->eventhih->gett(),
                        'duedates' => $this->eventhih->duedate()
                );
                $this->load->view("layout/header");
                $this->load->view("event", $data);
                $this->load->view("layout/footer");
        
            }



            public function detailss($id = 0){

                $data = array(
                    'event' => $this->eventhih->get(),
                    'events' => $this->eventhih->gett(),
                    'duedates' => $this->eventhih->duedate(),
                    'deta' => $this->eventhih->detailsss($id)
                );

                $this->load->view("layout/header");
                $this->load->view("detailevent", $data);
                $this->load->view("layout/footer");

            }

            public function detail($id = 0){
                
                $data = array(
                    'event' => $this->eventhih->get(),
                    'events' => $this->eventhih->gett(),
                    'duedates' => $this->eventhih->duedate(),
                    'det' =>$this->eventhih->detail($id),
                    'deta' =>$this->eventhih->detailsss($id)
                );
                $this->load->view("layout/header");
                $this->load->view("detailevent", $data);
                $this->load->view("layout/footer");
            }


        
            
    }
?>