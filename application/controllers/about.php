<?php
    class About extends CI_Controller {
      
            function __construct(){
                parent::__construct();
                $this->load->model('abouthih');
            }
            
        
            public function index(){ 
                $data = array(
                        'about' => $this->abouthih->get(),
                        'visi' => $this->abouthih->visi(),
                        'misi' => $this->abouthih->misi()
                       
                );
                $this->load->view("layout/header");
                $this->load->view("about", $data);
                $this->load->view("layout/footer");
        
            }

          
            
        
    }
?>