<?php
    class Gallery extends CI_Controller {
      
            function __construct(){
                parent::__construct();
                $this->load->model('galleryhih');
            }
            
        
            public function index(){ 
                $data = array(
                        'gallery' => $this->galleryhih->get()
                       
                );
                $this->load->view("layout/header");
                $this->load->view("gallery", $data);
                $this->load->view("layout/footer");
        
            }

          
            
        
    }
?>