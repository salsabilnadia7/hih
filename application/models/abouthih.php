<?php 
    class Abouthih extends CI_Model {
        
        //fungsi untuk mendapatkan data
        public function get() {
            $this->load->database();
            return $this->db->get("tb_preview")->result();
        }

        public function visi() {
            $this->load->database();
            return $this->db->get("tb_visi")->result();
        }

        public function misi() {
            $this->load->database();
            return $this->db->get("tb_misi")->result();
        }
    }
?>