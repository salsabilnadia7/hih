<?php 
    class Homehih extends CI_Model {
        
        //fungsi untuk mendapatkan data
        public function get() {
            $this->load->database();
            return $this->db->get("tb_preview")->result();
        }

        public function gett() {
            $this->load->database();
            return $this->db->get_where("tb_event", array('status_event' => 'completed'))->result();
        }

        public function getsoon() {
            $this->load->database();
            return $this->db->get_where("tb_event", array('status_event' => 'coming_soon'))->result();
        }

        public function detail($id){
            $this->load->database();
            $this->db->where("id_event", $id);
            return $this->db->get("tb_event")->result();
        }

        public function preview() {
            $this->load->database();
            return $this->db->get("tb_preview")->result();
            
        }
    }
?>