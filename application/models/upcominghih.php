<?php 
    class Upcominghih extends CI_Model {
        
        //fungsi untuk mendapatkan data
        public function get() {
            $this->load->database();
            return $this->db->get_where("tb_event", array('status_event' => 'coming_soon'))->result();
        }
    }
?>