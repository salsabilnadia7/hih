<?php 
    class Eventhih extends CI_Model {
        
        //fungsi untuk mendapatkan data
        public function get() {
            $this->load->database();
            $this->db->order_by('tgl_event', 'DESC');
            return $this->db->get("tb_event")->result();
        }

        public function gett() {
            $this->load->database();
            return $this->db->get("tb_event")->result();
        }

       public function duedate(){
           
           $this->db->order_by('tgl_event', 'DESC');
           return $this->db->get("tb_event")->result();
       }


       public function detailsss($id){
        $this->load->database();
        $this->db->where("id_event", $id);
        return $this->db->get("tb_event")->result();
    }

        public function detail($id){
            $this->load->database();
            $this->db->select('*');
            $this->db->from('tb_event');
            $this->db->join('tb_galeri', 'tb_galeri.id_event = tb_event.id_event');
            $this->db->where("tb_event.id_event", $id);
            return $this->db->get()->result();
        }
    }
?>