<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Event_model extends CI_Model {

        // load db
        public function __construct()
        {
        parent::__construct();
        $this->load->database();
        }

        //listing event
        public function listing_event()
        {
        $this->db->select('*');
        $this->db->from('tb_event');
        $this->db->order_by('id_event');
        $query = $this->db->get();
        return $query->result();
        }

         // tambah event
         public function tambah_event($data)
         {
         $this->db->insert('tb_event',$data);
         }

         
        // delete event
        public function delete_event($data)
        {
          $this->db->where('id_event',$data['id_event']);
          $this->db->delete('tb_event',$data);
        }



  

  

  // ------------------------------------------------------------------------

}

/* End of file User_model.php */
/* Location: ./application/models/User_model.php */