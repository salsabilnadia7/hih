	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick-theme.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js"></script>



<style>
.main {
  font-family:Arial;
  width:800px;
  display:block;
  margin:0 auto;
}
h3 {
    background: #fff;
    color: #3498db;
    font-size: 36px;
    line-height: 100px;
    margin: 10px;
    padding: 2%;
    position: relative;
    text-align: center;
}
.action{
  display:block;
  margin:100px auto;
  width:100%;
  text-align:center;
}
.action a {
  display:inline-block;
  padding:5px 10px; 
  background:#f30;
  color:#fff;
  text-decoration:none;
}
.action a:hover{
  background:#000;
}


@media (min-width:100px) and (max-width: 600px){

    .main {
  font-family:Arial;
  width:400px;
  display:block;
  margin:0 auto;
}
}
</style>

<style>

/* BLOG */
section.blog {
  margin-top: 30px;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  font-weight: 400;
  color: white;
}

section.blog-title {
  width: 100%;
}


.blog-title {
  display: block;

}

.blog-title-content {
  padding-top: 93px;
  padding-bottom: 87px;
}

.blog-title-wrapper {
  width: 100%;
  background-image: url(<?php echo base_url("assetfe/img/banner/ourevent.png");?>);
  background-position: center center;
  background-repeat: no-repeat;
  background-size: cover;
  background-size: auto;
  position: relative;
  overflow: hidden;
}


.single-blog-title h1 {
  color: white;
}

.single-blog-title {
  color: white;
}

.article-title h1 {
  font-size: 36px;
  font-weight: 700;
  margin-bottom: 11px;
  letter-spacing: 0.5px;
  color: white;
}
</style>
	 
	 
	 
	 <!--================Header Menu Area =================-->
		<header class="header_area">
           	<div class="top_menu row m0">
           		<div class="container">
					<div class="float-left">
						<ul class="list header_social">
							<li><a href="#" style="color:black;">handinhand.id</a></li>
						</ul>
					</div>
					<div class="float-right">
						
						<a class="ac_btn" href="<?php echo site_url();?>">Hand In Hand</a>
						<a class="dn_btn" href="<?php echo site_url('contact');?>">Donate Now</a>
					</div>
           		</div>	
           	</div>	
            <div class="main_menu">
            	<nav class="navbar navbar-expand-lg navbar-light">
					<div class="container">
						<!-- Brand and toggle get grouped for better mobile display -->
						<a class="navbar-brand logo_h" href="<?php echo site_url();?>"><img src="<?php echo base_url("assetfe/img/logoss.png");?>" alt="" width="35%"></a>
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
							<ul class="nav navbar-nav menu_nav ml-auto">
								<li class="nav-item"><a class="nav-link" href="<?php echo site_url();?>">Home</a></li> 
								<li class="nav-item"><a class="nav-link" href="<?php echo site_url('about');?>">About</a></li>
								<li class="nav-item"><a class="nav-link" href="<?php echo site_url('ourlittlesalesforce');?>">Our Little Salesforce</a></li> 
								<li class="nav-item active"><a class="nav-link" href="<?php echo site_url('event');?>">Events</a></li> 
								<li class="nav-item"><a class="nav-link" href="<?php echo site_url('contact');?>">Contact</a></li>
							</ul>
							
						</div> 
					</div>
            	</nav>
            </div>
		</header>
		
		 <!--================Home Banner Area =================-->
     <section class="blog-title">
    <div class="blog-title-wrapper ">
      <div class="blog-title-overlay"></div>
      <div class="container">
        <div class="blog-title-content">
          <div class="single-blog-title">
            <div><br><br><br><br>
            <center><p style="color:white; font-family: 'Comfortaa', cursive; font-size:48px;">Our Events</p></center><br><br>
            <center><p style="color:white; font-family: 'Comfortaa', cursive; font-size:35px;">Detail Events</p></center><br><br>

			  <br><br>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

		<!--================End Home Banner Area =================-->
		

       
        <!--================Blog Area =================-->
        <section class="blog_area" style="background:#e4e4e4;"><br><br>
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="blog_right_sidebar" style="background:white;" >
                            <aside class="single_sidebar_widget search_widget">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search Posts" style="background:#e4e4e4;">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="lnr lnr-magnifier"></i></button>
                                    </span>
                                </div><!-- /input-group -->
                               
                            </aside>
                          
                            <aside class="single_sidebar_widget post_category_widget" >
                               <br>
                                <ul class="list cat-list">
								<?php  foreach($event as $events){ ?>
                                    <li>
                                        <a href="<?php echo site_url("event/detail/".$events->id_event);?>" class="d-flex justify-content-between">
										
											<p> 
											
											<?php echo $events->judul_event; ?><br>
											<?php echo date('d F Y', strtotime($events->tgl_event)); ?>
											
											
											</p>
											
										
                                        </a>
                                    </li>	
									<?php } ?>														
                                </ul>
                            </aside>
                           
						</div>
						
					</div>
					<div class="col-lg-8">
						
					<div class="container">
          
					<div class="main">
                <div class="slider slider-for">
                
                <?php  foreach($det as $detailss){?>
									
                  <div><img src="<?php echo base_url('assets/upload/image/'.$detailss->foto); ?>" width="100%"></div>
              
                  <?php } ?>
                </div>
                <div class="slider slider-nav">
                <?php  foreach($det as $data){?>
                  <div><img src="<?php echo base_url('assets/upload/image/'.$data->foto); ?>" width="100%"></div>
                  <?php } ?>
                </div>
                     </div>


                     <?php  foreach($deta as $barus){?>
									
                          <br><br>
                            <h2 style="color:black; font-family: 'Comfortaa', cursive;"> <?= $barus->judul_event ?></h2>
                            <p style="color:black; font-family: 'Comfortaa', cursive;"> <font color="black"> Event : <?php echo date('d F Y', strtotime($barus->tgl_event)); ?></font></p>
									        	<p style="color:black; font-family: 'Comfortaa', cursive;"> <font color="black"> Post &nbsp; :  <?php echo date('d F Y', strtotime($barus->tgl_post)); ?></font></p>
                            <p align="justify" style="color:black; font-family: 'Comfortaa', cursive;"> <font color="black">  <?php echo $barus->deskripsi_event; ?></font></p> 

                    
                          
                           
                  <?php } ?>





					</div>


					</div>
                </div>
            </div><br><br>
		</section>
		



<script>
              
    $('.slider-for').slick({
   slidesToShow: 1,
   slidesToScroll: 1,
   arrows: false,
   fade: true,
   asNavFor: '.slider-nav'
 });
 $('.slider-nav').slick({
   slidesToShow: 3,
   slidesToScroll: 1,
   asNavFor: '.slider-for',
   dots: true,
   focusOnSelect: true
 });

 $('a[data-slide]').click(function(e) {
   e.preventDefault();
   var slideno = $(this).data('slide');
   $('.slider-nav').slick('slickGoTo', slideno - 1);
 });

</script>