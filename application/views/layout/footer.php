 <!-- <style>
.fa {
  padding: 18px;
  font-size: 18px;
  width: 50px;
  text-align: center;
  text-decoration: none;
}

/* Add a hover effect if you want */
.fa:hover {
  opacity: 0.9;
}

/* Set a specific color for each brand */

/* Facebook */
.fa-facebook {
  background: #3B5998;
  color: white;
}

.fa-twitter {
  background: #55ACEE;
  color: white;
}

.fa-instagram {
  background: #ff6600;
  color: white;
}

.fa-youtube {
  background: #bb0000;
  color: white;
   padding: 18px;
  font-size: 18px;
  width: 50px;
  text-align: center;
  text-decoration: none;
}
 </style> -->
 
 <!--================ start footer Area  =================-->	
 <footer class="footer-area section_gap" style="background-color:#686565; width:100%;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4  col-md-4 col-sm-4">
                        <div class="single-footer-widget">
                            <h6 class="footer_title">Social Media</h6>
                            <p>
                            <a href="https://www.instagram.com/handinhand.jkt/?hl=id" class="fa fa-instagram"
                            style="background: #ff6600; color: white; padding: 18px; font-size: 18px;
                                   width: 50px; text-align: center; text-decoration: none;"></a>
                            <a href="https://www.youtube.com/channel/UCvSIFWJD8nbxtqtYoDjM4aghttps://www.youtube.com/channel/UCvSlFWJD8nbxtqtYoDjM4ag?app=desktop" class="fa fa-youtube" 
                            style="background: #bb0000; color: white; padding: 18px; font-size: 18px; width: 50px;
                                   text-align: center; text-decoration: none;"></a> </p>
                        </div>
                    </div>

                    <div class="col-lg-2  col-md-2 col-sm-2">

                    </div>
                    
                    <div class="col-lg-6  col-md-6 col-sm-6">
                        <div class="single-footer-widget">
                            <h6 class="footer_title">Navigation Links</h6>
                            <div class="row">
                                <div class="col-4">
                                    <ul class="list">
                                        <li><a href="<?php echo site_url();?>" style="color:white; font-family: 'Comfortaa', cursive;">Home</a></li>
                                        <li><a href="<?php echo site_url('about');?>" style="color:white; font-family: 'Comfortaa', cursive;">About Us</a></li>
                                        <li><a href="<?php echo site_url('ourlittlesalesforce');?>" style="color:white; font-family: 'Comfortaa', cursive;">Our Little Salesforce</a></li>
                                       
                                    </ul>
                                </div>
                                <div class="col-6">
                                    <ul class="list">
                                        <li><a href="<?php echo site_url('event');?>" style="color:white;">Events</a></li>
                                        <li><a href="<?php echo site_url('contact');?>" style="color:white;">Contact</a></li>
                                    </ul>
                                </div>										
                            </div>							
                        </div>
                    </div>	
                    <p class="col-lg-12 col-md-8 footer-text m-0" style="color:white; font-family: 'Comfortaa', cursive;"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    <strong>Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | handinhand.id</strong></a>					
            </div>
        </footer>
		<!--================ End footer Area  =================-->
        
        
        
        
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="<?php echo base_url("assetfe/js/jquery-3.2.1.min.js");?>"></script>
        <script src="<?php echo base_url("assetfe/js/popper.js");?>"></script>
        <script src="<?php echo base_url("assetfe/js/bootstrap.min.js");?>"></script>
        <script src="<?php echo base_url("assetfe/js/stellar.js");?>"></script>
        <script src="<?php echo base_url("assetfe/vendors/lightbox/simpleLightbox.min.js");?>"></script>
        <script src="<?php echo base_url("assetfe/vendors/nice-select/js/jquery.nice-select.min.js");?>"></script>
        <script src="<?php echo base_url("assetfe/vendors/isotope/imagesloaded.pkgd.min.js");?>"></script>
        <script src="<?php echo base_url("assetfe/vendors/isotope/isotope-min.js");?>"></script>
        <script src="<?php echo base_url("assetfe/vendors/owl-carousel/owl.carousel.min.js");?>"></script>
        <script src="<?php echo base_url("assetfe/js/jquery.ajaxchimp.min.js");?>"></script>
        <script src="<?php echo base_url("assetfe/js/mail-script.js");?>"></script>
        <script src="<?php echo base_url("assetfe/js/theme.js");?>"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
        <script src="<?php echo base_url("assetfe/js/gmaps.min.js");?>"></script>
        <script src="<?php echo base_url("assetfe/js/theme.js");?>"></script>
    </body>
</html>