<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="img/favicon.png" type="image/png">
        <title>handinhand.id</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?php echo base_url("assetfe/css/bootstrap.css");?>">
        <link rel="stylesheet" href="<?php echo base_url("assetfe/vendors/linericon/style.css");?>">
        <link rel="stylesheet" href="<?php echo base_url("assetfe/css/font-awesome.min.css");?>">
        <link rel="stylesheet" href="<?php echo base_url("assetfe/vendors/owl-carousel/owl.carousel.min.css");?>">
        <link rel="stylesheet" href="<?php echo base_url("assetfe/vendors/lightbox/simpleLightbox.css");?>">
        <link rel="stylesheet" href="<?php echo base_url("assetfe/vendors/nice-select/css/nice-select.css");?>">
        <link rel="stylesheet" href="<?php echo base_url("assetfe/vendors/animate-css/animate.css");?>">
        <!-- main css -->
        <link rel="stylesheet" href="<?php echo base_url("assetfe/css/style.css");?>">
        <link rel="stylesheet" href="<?php echo base_url("assetfe/css/responsive.css");?>">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Comfortaa&display=swap" rel="stylesheet">
	</head>
	

	<body>
        

