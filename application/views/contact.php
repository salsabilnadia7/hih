<style>
body {
  font-family: Arial, Helvetica, sans-serif;
}

.flip-box {
  background-color: transparent;
  width: 300px;
  height: 200px;
  border: 1px solid #f1f1f1;
  perspective: 1000px;
}

.flip-box-inner {
  position: relative;
  width: 100%;
  height: 100%;
  text-align: center;
  transition: transform 0.8s;
  transform-style: preserve-3d;
}



.flip-box-front, .flip-box-back {
  position: absolute;
  width: 100%;
  height: 100%;
  backface-visibility: hidden;
}

.flip-box-front {
  background-color: #bbb;
  color: black;
}

.flip-box-back {
  background-color: dodgerblue;
  color: white;
}
</style>  



<style>

/* BLOG */
section.blog {
  margin-top: 30px;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  font-weight: 400;
  color: white;
}

section.blog-title {
  width: 100%;
}


.blog-title {
  display: block;

}

.blog-title-content {
  padding-top: 93px;
  padding-bottom: 87px;
}

.blog-title-wrapper {
  width: 100%;
  background-image: url(<?php echo base_url("assetfe/img/banner/contactus.png");?>);
  background-position: center center;
  background-repeat: no-repeat;
  background-size: cover;
  background-size: auto;
  position: relative;
  overflow: hidden;
}


.single-blog-title h1 {
  color: white;
}

.single-blog-title {
  color: white;
}

.article-title h1 {
  font-size: 36px;
  font-weight: 700;
  margin-bottom: 11px;
  letter-spacing: 0.5px;
  color: white;
}
</style>
	 
 
 
 
 <!--================Header Menu Area =================-->
		<header class="header_area">
           	<div class="top_menu row m0">
           		<div class="container">
					<div class="float-left">
						<ul class="list header_social">
						     <li><a href="#" style="color:black; font-family: 'Comfortaa', cursive; font-size:12px;">handinhand.id</a></li>
						</ul>
					</div>
					<div class="float-right">
						
						<a class="ac_btn" href="#">Hand In Hand</a>
						<a class="dn_btn" href="#">Donate Now</a>
					</div>
           		</div>	
           	</div>	
            <div class="main_menu">
            	<nav class="navbar navbar-expand-lg navbar-light">
					<div class="container">
						<!-- Brand and toggle get grouped for better mobile display -->
						<a class="navbar-brand logo_h" href="<?php echo site_url();?>"><img src="<?php echo base_url("assetfe/img/logoss.png");?>" alt="" width="35%"></a>
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
							<ul class="nav navbar-nav menu_nav ml-auto">
								<li class="nav-item"><a class="nav-link" href="<?php echo site_url();?>">Home</a></li> 
								<li class="nav-item"><a class="nav-link" href="<?php echo site_url('about');?>">About</a></li>
								<li class="nav-item"><a class="nav-link" href="<?php echo site_url('ourlittlesalesforce');?>">Our Little Salesforce</a></li> 
								<li class="nav-item"><a class="nav-link" href="<?php echo site_url('event');?>">Events</a></li> 
								<li class="nav-item active"><a class="nav-link" href="<?php echo site_url('contact');?>">Contact</a></li>
							</ul>
							
						</div> 
					</div>
            	</nav>
            </div>
		</header>
		
		<section class="blog-title">
    <div class="blog-title-wrapper ">
      <div class="blog-title-overlay"></div>
      <div class="container">
        <div class="blog-title-content">
          <div class="single-blog-title">
            <div><br><br><br><br>
            <center><p style="color:white; font-family: 'Comfortaa', cursive; font-size:48px;">Contact Us</p></center><br><br>

			  <br><br>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


		  
        <!--================Contact Area =================-->
        <section class="contact_area p_120" style="background:#E4E4E4">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="contact_info"><br><br>
                            <div class="info_item">
								<h6 style="font-family: 'Comfortaa', cursive;"><i class="fa fa-money" style="font-size:30px; "></i>
                                Bank Transfer <p>Perkumpulan Hand In Hand 5235127234 (BCA)</p></h6>
								
                            </div>
						
                            
                        </div>
										</div>
						<div class="col-lg-3">
                        <div class="contact_info"><br><br>
                            <div class="info_item">
							<h6  style="font-family: 'Comfortaa', cursive;"><a href="https://www.instagram.com/handinhand.jkt/?hl=id" target="_blank"><i class="fa fa-instagram" style="font-size:30px;"></i>
                                Instagram <p>@handinhand.jkt</p></h6></a>
                            </div>
                           
                            
                        </div>
							</div>
						
						<div class="col-lg-3">
                        <div class="contact_info"><br><br>
                            <div class="info_item">
								<h6  style="font-family: 'Comfortaa', cursive;"><i class="fa fa-envelope" style="font-size:30px;"></i>
                                Email <p>hih.jkt@gmail.com</p></h6>
                            </div>
                           
                            
                        </div>
					</div>
					
					<div class="col-lg-3">
                        <div class="contact_info"><br><br>
                            <div class="info_item">
							<h6  style="font-family: 'Comfortaa', cursive;"><a href="https://www.youtube.com/channel/UCvSlFWJD8nbxtqtYoDjM4ag?app=desktop"  target="_blank"><i class="fa fa-youtube" style="font-size:30px;"></i>
                                youtube <p>Hand In Hand Indonesia</p></a></h6>
                                
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!--================Contact Area =================-->