<section class="banner_area">
            <div class="banner_inner d-flex align-items-center">
            	<div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
				<div class="container">
					<div class="banner_content text-center">
						<h2>Upcoming Events</h2>
						<div class="page_link">
							<a href="<?php echo site_url('');?>">Home</a>
							<a href="<?php echo site_url('upcoming');?>">Events</a>
						</div>
					</div>
				</div>
            </div>
        </section>



         <!--================Event Main Area =================-->
         <section class="event_main_area p_120">
        	<div class="container">
        		<div class="row event_main_inner">
                
				<?php  foreach($upcoming as $keg){ ?>
        			<div class="col-lg-6">
        				<div class="event_item">
        					<div class="media">
        						<div class="d-flex">
        							<img src="<?php echo base_url('assets/upload/event/'.$keg->foto_event); ?>" alt="" width="90%">
        						</div>
        						<div class="media-body">
        							<a href="#"><i class="fa fa-calendar"></i>&nbsp;<?php echo date('d F Y', strtotime($keg->tgl_event)); ?></a>
        							<a href="<?php echo site_url("home/detail/".$keg->id_event); ?>"><h4><?php echo $keg->judul_event ?></h4></a>
        							<p align="justify"><?php echo limit_words($keg->deskripsi_event, 15) ?> .&nbsp;.&nbsp;
                                .&nbsp;.&nbsp;.&nbsp;.</p>
        						</div>
        					</div>
        				</div>
        			</div>

				<?php } ?>


        		</div>
        	</div>
        </section>
        <!--================End Event Main Area =================-->
        

		<!-- FUNCTION -->
		
		<?php
		function limit_words($string, $word_limit){
			$words = explode(" ",$string);
			return implode(" ",array_splice($words,0,$word_limit));
		}
		?>

		<!-- END FUNCTION -->