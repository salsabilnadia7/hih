
 
 
 
 <!--================Home Banner Area =================-->
 <section class="banner_area">
            <div class="banner_inner d-flex align-items-center">
            	<div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
				<div class="container">
					<div class="banner_content text-center">
						<h2 style="font-family: 'ALIN_KID', sans-serif;">Hand in Hand Gallery</h2>
						<div class="page_link">
							<a href="<?php echo site_url();?>" style="font-family: 'ALIN_KID', sans-serif;">Home</a>
							<a href="<?php echo site_url('gallery');?>" style="font-family: 'ALIN_KID', sans-serif;">Gallery</a>
						</div>
					</div>
				</div>
            </div>
        </section>


<!--           
        <!--================Gallery Area =================-->
        <section class="gallery_area p_120">
            <div class="container">

                <div class="row gallery_inner imageGallery1">
                <?php  foreach($gallery as $datafoto){ ?>
                    <div class="col-md-4 col-sm-6 gallery_item">
                        <div class="gallery_img">
                        <div class="thumbnail">
                            <img src="<?php echo base_url('assets/upload/image/'.$datafoto->foto); ?>" alt="" width="100%">
                            <div class="caption">
                           
                                
                                </div>
                           
                            <div class="hover">
                            	<a class="light" href="<?php echo base_url('assets/upload/image/'.$datafoto->foto); ?>"><i class="fa fa-expand"></i></a>
                            </div>
                        </div>
                    </div>
                    </div>

                <?php } ?>
                  
                </div>
            </div>
        </section>
       