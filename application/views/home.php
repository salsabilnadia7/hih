		
		<!--================Header Menu Area =================-->
         <header class="header_area">
           	<div class="top_menu row m0">
           		<div class="container">
					<div class="float-left">
						<ul class="list header_social">
							<li><a href="#" style="color:black;">handinhand.id</a></li>
						</ul>
					</div>
					<div class="float-right">
						<a class="ac_btn" href="<?php echo site_url();?>">Hand In Hand</a>
						<a class="dn_btn" href="<?php echo site_url('contact');?>">Donate Now</a>
					</div>
           		</div>	
           	</div>	
            <div class="main_menu">
            	<nav class="navbar navbar-expand-lg navbar-light">
					<div class="container">
						<!-- Brand and toggle get grouped for better mobile display -->
						<a class="navbar-brand responsive" href="<?php echo site_url();?>"><img src="<?php echo base_url("assetfe/img/logoss.png");?>" alt="" width="35%"></a>
					
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" style="">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
							<ul class="nav navbar-nav menu_nav ml-auto">
								<li class="nav-item active"><a class="nav-link" href="<?php echo site_url();?>">Home</a></li> 
								<li class="nav-item"><a class="nav-link" href="<?php echo site_url('about');?>">About</a></li>
								<li class="nav-item"><a class="nav-link" href="<?php echo site_url('ourlittlesalesforce');?>">Our Little Salesforce</a></li> 
								<li class="nav-item"><a class="nav-link" href="<?php echo site_url('event');?>">Events</a></li> 
								<li class="nav-item"><a class="nav-link" href="<?php echo site_url('contact');?>">Contact</a></li>
							</ul>
						</div> 
					</div>
            	</nav>
            </div>
        </header>
 
 
 
 
 
 <!--================Home Banner Area =================-->
 <section class="home_banner_area" style="background:#686565;">
            <div class="banner_inner d-flex align-items-center">
            	<div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
				<div class="container">
					<div class="banner_content text-center" style=" padding: 100px 0;
  border: 3px solid white; background-color: rgba(0,0,0,0.5);">
						<h3 style="font-family: 'Comfortaa', cursive;">Kids For Kids</h3>
						<p style="font-family: 'Comfortaa', cursive;"><b><font color="white">
						   <!-- Fostering a love of community service <br> from a young age to learn empathy
                           kindness and the importance of giving back. -->
						  
							   
							 <p style="padding-left:150px; padding-right:150px">
							 
							 <?php  foreach($homes as $preview){ 
								 
								 
								 echo $preview->konten_prev;
								 
								 
							 } ?>

							 
							 
							 
							   </p>
							

							
							


						   </font></b></p>
						   <a class="main_btn" href="<?php echo site_url('about');?>" style="font-family: 'Comfortaa', cursive;">More About Us</a><br><br>
					</div>
					
					
				</div>
            </div>	
					</div>
				</div>
            </div>
        </section>