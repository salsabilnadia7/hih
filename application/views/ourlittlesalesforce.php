<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick-theme.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js"></script>

<style>
/******************* Timeline Demo - 6 *****************/
.timeline {
    list-style: none;
    padding: 20px 0 20px;
    position: relative;
}

.timeline:before {
  top: 0;
  bottom: 0;
  position: absolute;
  content: " ";
  width: 3px;
  background-color: black;
  left: 50%;
  margin-left: -1.5px;
}

.timeline > li {
  margin-bottom: 20px;
  position: relative;
}

.timeline > li:before,
.timeline > li:after {
  content: " ";
  display: table;
}

.timeline > li:after {
  clear: both;
}

.timeline > li:before,
.timeline > li:after {
  content: " ";
  display: table;
}

.timeline > li:after {
  clear: both;
}

.timeline > li > .timeline-panel {
  width: 46%;
  float: left;
  border: 1px solid #d4d4d4;
  border-radius: 2px;
  padding: 20px;
  position: relative;
  -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
  box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
}

.timeline > li > .timeline-panel:before {
  position: absolute;
  top: 26px;
  right: -15px;
  display: inline-block;
  border-top: 15px solid transparent;
  border-left: 15px solid #ccc;
  border-right: 0 solid #ccc;
  border-bottom: 15px solid transparent;
  content: " ";
}

.timeline > li > .timeline-panel:after {
  position: absolute;
  top: 27px;
  right: -14px;
  display: inline-block;
  border-top: 14px solid transparent;
  border-left: 14px solid #fff;
  border-right: 0 solid #fff;
  border-bottom: 14px solid transparent;
  content: " ";
}

.timeline > li > .timeline-badge {
  color: #fff;
  width: 50px;
  height: 50px;
  line-height: 50px;
  font-size: 1.4em;
  text-align: center;
  position: static;
  top: 16px;
  left: 50%;
  margin-left: 560px;
  background-color: #f2f2f2;
  z-index: 100;
  border-top-right-radius: 50%;
  border-top-left-radius: 50%;
  border-bottom-right-radius: 50%;
  border-bottom-left-radius: 50%;
}

.timeline > li.timeline-inverted > .timeline-panel {
  float: right;
}

.timeline > li.timeline-inverted > .timeline-panel:before {
  border-left-width: 0;
  border-right-width: 15px;
  left: -15px;
  right: auto;
}

.timeline > li.timeline-inverted > .timeline-panel:after {
  border-left-width: 0;
  border-right-width: 14px;
  left: -14px;
  right: auto;
}

.timeline-badge.primary {
    background-color: #2e6da4 !important;
}

.timeline-badge.success {
    background-color: #3f903f !important;
}

.timeline-badge.warning {
    background-color: #f0ad4e !important;
}

.timeline-badge.danger {
    background-color: #d9534f !important;
}

.timeline-badge.info {
    background-color: #5bc0de !important;
}

.timeline-title {
    margin-top: 0;
    color: inherit;
}

.timeline-body > p,
.timeline-body > ul {
    margin-bottom: 0;
}

.timeline-body > p + p {
  margin-top: 5px;
}

@media (max-width: 1200px) {
  ul.timeline:before {
    left: 40px;
  }
  
  ul.timeline > li > .timeline-panel {
    width: calc(100% - 90px);
    width: -moz-calc(100% - 90px);
    width: -webkit-calc(100% - 90px);
  }
  
  ul.timeline > li > .timeline-badge {
    left: 15px;
    margin-left: 15px;
    top: 16px;
  }
  
  ul.timeline > li > .timeline-panel {
    float: right;
  }
  
  ul.timeline > li > .timeline-panel:before {
    border-left-width: 0;
    border-right-width: 15px;
    left: -15px;
    right: auto;
  }
  
  ul.timeline > li > .timeline-panel:after {
    border-left-width: 0;
    border-right-width: 14px;
    left: -14px;
    right: auto;
  }
}



.responsive {
  width: 100%;
  height: auto;
}


*,
*::before,
*::after {
	margin: 0;
	padding: 0;
	outline: none;
	box-sizing: border-box;
}


/* Useful Classes */
.xy-center {
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
}

.transition {
	transition: all 350ms ease-in-out;
}

.r-3-2 {
	width: 100%;
	padding-bottom: 66.667%;
	background-color: #ddd;
}

.image-holder {
	background-size: cover;
	background-position: center center;
	background-repeat: no-repeat;
}

/* Main Styles */
.gallery-wrapper {
	position: relative;
	overflow: hidden;
}

.gallery {
	position: relative;
	white-space: nowrap;
	font-size: 0;
}

.item-wrapper {
	cursor: pointer;
	width: 23%; /* arbitrary value */
	display: inline-block;
	background-color: white;
}

.gallery-item { opacity: 0.5; }
.gallery-item.active { opacity: 1; }

.controls {
	font-size: 0;
	border-top: none;
}
.move-btn {
	display: inline-block;
	width: 50%;
	border: none;
  color: black;
	background-color: transparent;
	/* padding: 0.2em 1.5em; */
}
.move-btn:first-child {border-right: none;}
.move-btn.left  { cursor: w-resize; }
.move-btn.right { cursor: e-resize; }

</style>


<style>
.main {
  font-family:Arial;
  width:800px;
  display:block;
  margin:0 auto;
}
h3 {
    background: #fff;
    color: #3498db;
    font-size: 36px;
    line-height: 100px;
    margin: 10px;
    padding: 2%;
    position: relative;
    text-align: center;
}
.action{
  display:block;
  margin:100px auto;
  width:100%;
  text-align:center;
}
.action a {
  display:inline-block;
  padding:5px 10px; 
  background:#f30;
  color:#fff;
  text-decoration:none;
}
.action a:hover{
  background:#000;
}


@media (min-width:100px) and (max-width: 600px){

    .main {
  font-family:Arial;
  width:400px;
  display:block;
  margin:0 auto;
}
}

</style>


<style>

/* BLOG */
section.blog {
  margin-top: 30px;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  font-weight: 400;
  color: white;
}

section.blog-title {
  width: 100%;
}


.blog-title {
  display: block;

}

.blog-title-content {
  padding-top: 93px;
  padding-bottom: 87px;
}

.blog-title-wrapper {
  width: 100%;
  background-image: url(<?php echo base_url("assetfe/img/banner/littlesalesforcebanner.png");?>);
  background-position: center center;
  background-repeat: no-repeat;
  background-size: cover;
  background-size: auto;
  position: relative;
  overflow: hidden;
}

/* .blog-title-overlay {
  position: absolute;
  top: 0px;
  right: 0px;
  bottom: 0px;
  left: 0px;
  background-color: rgba(0,0,0,0.5);   

} */

.single-blog-title h1 {
  color: white;
}

.single-blog-title {
  color: white;
}

.article-title h1 {
  font-size: 36px;
  font-weight: 700;
  margin-bottom: 11px;
  letter-spacing: 0.5px;
  color: white;
}
</style>


    <!--================Header Menu Area =================-->
	<header class="header_area">
           	<div class="top_menu row m0">
           		<div class="container">
					<div class="float-left">
						<ul class="list header_social">
                <li><a href="#" style="color:black;">handinhand.id</a></li>
						</ul>
					</div>
					<div class="float-right">
						
						<a class="ac_btn" href="<?php echo site_url();?>">Hand In Hand</a>
						<a class="dn_btn" href="<?php echo site_url('contact');?>">Donate Now</a>
					</div>
           		</div>	
           	</div>	
            <div class="main_menu">
            	<nav class="navbar navbar-expand-lg navbar-light">
					<div class="container">
						<!-- Brand and toggle get grouped for better mobile display -->
						<a class="navbar-brand logo_h" href="index.html"><img src="<?php echo base_url("assetfe/img/logoss.png");?>" alt="" width="35%"></a>
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
							<ul class="nav navbar-nav menu_nav ml-auto">
								<li class="nav-item"><a class="nav-link" href="<?php echo site_url();?>">Home</a></li> 
								<li class="nav-item"><a class="nav-link" href="<?php echo site_url('about');?>">About</a></li>
								<li class="nav-item active"><a class="nav-link" href="<?php echo site_url('ourlittlesalesforce');?>">Our Little Salesforce</a></li> 
								<li class="nav-item"><a class="nav-link" href="<?php echo site_url('event');?>">Events</a></li> 
								<li class="nav-item"><a class="nav-link" href="<?php echo site_url('contact');?>">Contact</a></li>
							</ul>
							
						</div> 
					</div>
            	</nav>
            </div>
		</header>





 <!--================Home Banner Area =================-->
 <section class="blog-title">
    <div class="blog-title-wrapper ">
      <div class="blog-title-overlay"></div>
      <div class="container">
        <div class="blog-title-content">
          <div class="single-blog-title">
            <div><br><br><br><br>
            <center><p style="color:white; font-family: 'Comfortaa', cursive; font-size:48px;">Our Little Salesforce</p></center><br><br>

			  <br><br>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
		<!--================End Home Banner Area =================-->




 <!--================Welcome Area =================-->
 <section class="welcome_area p_120" style="background-color:#e4e4e4;">
        	<div class="container">
        		<div class="row welcome_inner">
        			<div class="col-lg-12">
					<center><h1 style="color:black; font-family: 'Comfortaa', cursive;">How We Fundraise</h1></center><br><br>
					<div class="row">
        <ul class="timeline">
            <li>
              <div class="timeline-badge" style="background:#0161C3;"><i class="glyphicon glyphicon-hand-left"></i></div>
              <div class="timeline-panel img-2" style="background-color:#ffffff;">
                <div class="timeline-heading">
                  <h4 class="timeline-title"  style="font-family: 'Comfortaa', cursive; color:black;">

                  <?php  foreach($salesforce1 as $salesforce1){ ?>
								 
								 
                            <?= $salesforce1->judul; ?>
                           
								 
								 
							      
                  
                  
                  </h4>
                 
                </div>
                <div class="timeline-body ">
                  <p align="left" style="font-family: 'Comfortaa', cursive; color:black;">
                  
                  <?= $salesforce1->deskripsi; ?>


                      <?php } ?>
                  
                  
                  </p>
                </div>
              </div>
            </li>
            <li class="timeline-inverted">
              <div class="timeline-badge" style="background:#FDDE00;"><i class="glyphicon glyphicon-chevron-right"></i></div>
              <div class="timeline-panel" style="background-color:#ffffff;">
                <div class="timeline-body">
                  <p  style="font-family: 'Comfortaa', cursive; color:black;">
                  <?php  foreach($salesforce2 as $salesforce2){ 
								 
                        echo $salesforce2->judul; 
                        echo $salesforce2->deskripsi; 
                 
                 
                  } ?>
                  </p>
                </div>
              </div>
            </li>
            <li>
              <div class="timeline-badge" style="background:#FC0202;"><i class="glyphicon glyphicon-eye-open"></i></div>
              <div class="timeline-panel" style="background-color:#ffffff;">
                <div class="timeline-heading">
                  <h4 class="timeline-title"  style="font-family: 'Comfortaa', cursive; color:black;">
                  
                  <?php  foreach($salesforce3 as $salesforce3){ ?>
								 
								 
                 <?= $salesforce3->judul; ?>
                 
          
          
         
                  
                  </h4>
                </div>
                <div class="timeline-body">
          <p align="justify"  style="font-family: 'Comfortaa', cursive; color:black;">
                 
          <?= $salesforce3->deskripsi; ?>


                  <?php } ?>

                </p>
                </div>
              </div>
        </li>
        
    
          <li class="timeline-inverted">
              <div class="timeline-badge" style="background:#24DC00;"><i class="glyphicon glyphicon-chevron-right"></i></div>
              <div class="timeline-panel" style="background-color:#ffffff;">
          <div class="timeline-heading">
                  <h4 class="timeline-title"  style="font-family: 'Comfortaa', cursive; color:black;">
                  <?php  foreach($salesforce4 as $salesforce4){ ?>
								 
								 
                 <?= $salesforce4->judul; ?>
                  
                  </h4>
                </div>
                <div class="timeline-body">
                  <p  style="font-family: 'Comfortaa', cursive; color:black;">
                  
                  <?= $salesforce4->deskripsi; ?>


                        <?php } ?>
                  
                  </b></p>
                </div>
              </div>
            </li>  
        </ul> 
      </div>	<br><br>
      
      
      <div class="row">
  <!-- <div class="col-sm-"></div> -->

<div class="col-sm-12">
	
<div class="main">
                <div class="slider slider-for">
                  <div><img src="<?php echo base_url("assetfe/img/gallery/1.jpg");?>" width="100%"></div>
                  <div><img src="<?php echo base_url("assetfe/img/gallery/2.jpg");?>" width="100%"></div>
                  <div><img src="<?php echo base_url("assetfe/img/gallery/3.jpg");?>" width="100%"></div>
                  <div><img src="<?php echo base_url("assetfe/img/gallery/4.jpg");?>" width="100%"></div>
                  <div><img src="<?php echo base_url("assetfe/img/gallery/5.jpg");?>" width="100%"></div>
                  <div><img src="<?php echo base_url("assetfe/img/gallery/6.jpg");?>" width="100%"></div>
                  <div><img src="<?php echo base_url("assetfe/img/gallery/7.jpg");?>" width="100%"></div>
                  <div><img src="<?php echo base_url("assetfe/img/gallery/8.jpg");?>" width="100%"></div>
                </div>
                <div class="slider slider-nav">
                  <div><img src="<?php echo base_url("assetfe/img/gallery/1.jpg");?>" width="100%"></div>
                  <div><img src="<?php echo base_url("assetfe/img/gallery/2.jpg");?>" width="100%"></div>
                  <div><img src="<?php echo base_url("assetfe/img/gallery/3.jpg");?>" width="100%"></div>
                  <div><img src="<?php echo base_url("assetfe/img/gallery/4.jpg");?>" width="100%"></div>
                  <div><img src="<?php echo base_url("assetfe/img/gallery/5.jpg");?>" width="100%"></div>
                  <div><img src="<?php echo base_url("assetfe/img/gallery/6.jpg");?>" width="100%"></div>
                  <div><img src="<?php echo base_url("assetfe/img/gallery/7.jpg");?>" width="100%"></div>
                  <div><img src="<?php echo base_url("assetfe/img/gallery/8.jpg");?>" width="100%"></div>
                </div>
                     </div>






</div>
</div>


        <!--================End Welcome Area =================-->

	
 
           <br><br><br>
            <center><h1 style="color:black; font-family: 'Comfortaa', cursive;"> Our Partner </h1></center>
                <div class="row gallery_inner imageGallery1">
                    
                    <div class="col-md-12 col-sm-6 gallery_item">
                        <div class="gallery_img">
                           <!-- <center> <img src="<?php echo base_url("assetfe/img/clients-logo/ASHI.png");?>" width="40%" alt="">
                            <img src="<?php echo base_url("assetfe/img/clients-logo/DL.png");?>" width="40%" alt="">
                            <img src="<?php echo base_url("assetfe/img/clients-logo/BRIOS.png");?>" width="40%" alt="">
                            <img src="<?php echo base_url("assetfe/img/clients-logo/BENGKEL.png");?>" width="40%" alt=""> -->

                            <table>
                              <tr>
                                  <td><img src="<?php echo base_url("assetfe/img/clients-logo/ASHI.png");?>" width="180%" alt=""></td>
                                  <td> <img src="<?php echo base_url("assetfe/img/clients-logo/DL.png");?>" width="180%" alt=""></td>
                                  <td>  <img src="<?php echo base_url("assetfe/img/clients-logo/BRIOS.png");?>" width="180%" alt=""></td>
                                  <td> <img src="<?php echo base_url("assetfe/img/clients-logo/BENGKEL.png");?>" width="180%" alt=""></td>
                                  <td> <img src="<?php echo base_url("assetfe/img/clients-logo/uc100.png");?>" width="180%" alt=""></td>
                              </tr>
                            </table>
                           
                            
                        </div>
				

			
					
       
      
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        
 
 
 
  
  <script>
              
              $('.slider-for').slick({
   slidesToShow: 1,
   slidesToScroll: 1,
   arrows: false,
   fade: true,
   asNavFor: '.slider-nav'
 });
 $('.slider-nav').slick({
   slidesToShow: 3,
   slidesToScroll: 1,
   asNavFor: '.slider-for',
   dots: true,
   focusOnSelect: true
 });

 $('a[data-slide]').click(function(e) {
   e.preventDefault();
   var slideno = $(this).data('slide');
   $('.slider-nav').slick('slickGoTo', slideno - 1);
 });
              </script>

