<style>

/* BLOG */
section.blog {
  margin-top: 30px;
  font-family: 'Roboto', sans-serif;
  font-size: 14px;
  font-weight: 400;
  color: white;
}

section.blog-title {
  width: 100%;
}


.blog-title {
  display: block;

}

.blog-title-content {
  padding-top: 93px;
  padding-bottom: 87px;
}

.blog-title-wrapper {
  width: 100%;
  background-image: url(<?php echo base_url("assetfe/img/banner/aboutus.png");?>);
  background-position: center center;
  background-repeat: no-repeat;
  background-size: cover;
  background-size: auto;
  position: relative;
  overflow: hidden;
}



.single-blog-title h1 {
  color: white;
}

.single-blog-title {
  color: white;
}

.article-title h1 {
  font-size: 36px;
  font-weight: 700;
  margin-bottom: 11px;
  letter-spacing: 0.5px;
  color: white;
}
</style>
		
		<!--================Header Menu Area =================-->
         <header class="header_area">
           	<div class="top_menu row m0">
           		<div class="container">
					<div class="float-left">
						<ul class="list header_social">
						     <li><a href="#" style="color:black;">handinhand.id</a></li>
						</ul>
					</div>
					<div class="float-right">
						
						<a class="ac_btn" href="<?php echo site_url();?>">Hand In Hand</a>
						<a class="dn_btn" href="<?php echo site_url('contact');?>">Donate Now</a>
					</div>
           		</div>	
           	</div>	
            <div class="main_menu">
            	<nav class="navbar navbar-expand-lg navbar-light">
					<div class="container">
						<!-- Brand and toggle get grouped for better mobile display -->
						<a class="navbar-brand logo_h" href="<?php echo site_url();?>"><img src="<?php echo base_url("assetfe/img/logoss.png");?>" alt="" width="35%"></a>
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
							<ul class="nav navbar-nav menu_nav ml-auto">
								<li class="nav-item"><a class="nav-link" href="<?php echo site_url();?>">Home</a></li> 
								<li class="nav-item active"><a class="nav-link" href="<?php echo site_url('about');?>">About</a></li>
								<li class="nav-item"><a class="nav-link" href="<?php echo site_url('ourlittlesalesforce');?>">Our Little Salesforce</a></li> 
								<li class="nav-item"><a class="nav-link" href="<?php echo site_url('event');?>">Events</a></li> 
								<li class="nav-item"><a class="nav-link" href="<?php echo site_url('contact');?>">Contact</a></li>
							</ul>
							
						</div> 
					</div>
            	</nav>
            </div>
		</header>
		
		 <!--================Home Banner Area =================-->
		 <!-- <section class="banner_area">
            <div class="banner_inner d-flex align-items-center">
            	<div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
				<div class="container">
					<div class="banner_content text-center">
						<h2>About Us</h2>
					</div>
				</div>
            </div>
		</section> -->
		
		<section class="blog-title">
    <div class="blog-title-wrapper ">
      <div class="blog-title-overlay"></div>
      <div class="container">
        <div class="blog-title-content">
          <div class="single-blog-title">
            <div><br><br><br><br>
            <center><p style="color:white; font-family: 'Comfortaa', cursive; font-size:48px;">About Us</p></center><br><br>

			  <br><br>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
		<!--================End Home Banner Area =================-->

		
		
  
            <!-- Start service Area -->
			<section class="service-area section-gap" id="about" style="background-color:#E4E4E4;">
				<div class="container"><br><br>
				<div class="title text-center">
								<h1 class="mb-10" style="font-family: 'Comfortaa', cursive; weight:bold; color:black;">Our Core Values </h1>
							
							</div>
					<div class="row">

						<div class="col-lg-6"><br><br>
							<div class="image-holder">
								<img src="<?php echo base_url("assetfe/img/banner/aboutt.png");?>" class="img-fluid img-2" alt=""
								style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);" width="100%">
							</div>
							<!-- </div> -->
							 						 	
                              
							<!-- </div>							 -->
						</div>

							<div class="col-lg-6"><br><br>
							<div class="image-holder">
							<div class="single-service img-2" style="background:white;  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
							<div class="container"><br>
								 <h3 style="font-family: 'Comfortaa', cursive;; color:black;"> Vision </h3>	
								 <p style="font-family: 'Comfortaa', cursive;;" align="left">
								 <?php  foreach($visi as $vision){ ?>
                            
											<p align="left" style="font-family: 'Comfortaa', cursive;; color:black;  font-size: 12px;"><?php echo $vision->visi ?> </p>
										
										
										<?php } ?><br>
								</p>
                              
							</div> </div> 
							</div>				
						

						<!-- <div class="col-lg-6" align="left"> -->
						<div class="image-holder">
							<div class="single-service img-2" style="background:white;  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
							<div class="container"><br>
							<h3 style="font-family: 'Comfortaa', cursive;; color:black;">  Mission </h3>	<br>
							<ul style="list-style-type:disc;">
							<?php  foreach($misi as $mision){ ?>
                            
								<li align="left" style="font-family: 'Comfortaa', cursive;; color:black; font-size: 12px;"><?php echo $mision->misi ?></li>
								<?php } ?><br><br>
								</ul>
														
							
						</div>
						</div> 
						</div>
						</div>
						
																
					</div>
				</div>	<br><br>
			</section>


	      
    
        